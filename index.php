<?php
/* Third Party Modules/Libraries */
require 'vendor/autoload.php';
/* App defaults */
$app = new \Koala\Slim(array(
    'view' => new \Koala\View));
/* Settings */
require 'app/settings.php';
/* Database */
$app->db = \Capsule\Database\Connection::make('main', $db, true);
/* Config */
$app->config(array(
    'debug' => $debug,
    'templates.path' => $templates));
/* Translation */
$app->translator = new \Translate\Core\ModelTranslator($app->db);
$app->mainLanguage = 'pt_BR';
/* Hooks */
$app->hook('slim.before.dispatch', function () use ($app) {
    /* Template context */
    $app->view->appendData(
        array(
            'site' => $app->site,
            'static' => $app->static,
            'media' => $app->media,
            'imageSize' => (5 * 1000000), // 5MB
            'params' => $app->request->params(),
            'request' => $app->request
        )
    );
});
/* Routes (Priority is top-bottom) */
require 'app/urls.php';
/* Run */
$app->run();