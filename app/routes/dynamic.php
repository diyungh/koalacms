<?php

$app->group('/admin', function() use ($app) {

    $app->group('/:child_class', function() use ($app) {

        # SCHEMA
        $app->get('/schema', function($child_class) use ($app){
            $schema = $app->db->getSchemaBuilder();
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_instance = new $_class();
            $schema->dropIfExists($_instance->table);
            $_instance->buildSchema();
        });
        # LIST
        $app->get('', function($child_class) use ($app) {
            // Paginate calc
            $itemsPerPage = 10;
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_viewClass = "\KoalaCMS\View\\" . ucwords($child_class) . 'View';
            $_viewClass = new $_viewClass();
            $items = call_user_func(array($_class, "onlyAvailable"))->Ordered();
            // If Searching...
            if($app->request->get('q')){
                $items = $items->Filter($app->request->get('q'));}
            // Paginate
            $_total = $items->count();
            $_results = $items->Paginated($itemsPerPage)->get();
            // Render
            $_params = array(
                    'items' => $_results,
                    'paginate' => calculatePages($_total, $itemsPerPage),
                    'model' => $child_class,
                    'listFields' => $_viewClass->listOrder,
                    'fields' => $_viewClass->getFields(true));
            try {
                $app->render('koalacms/admin/' . $child_class . '/list.html', $_params);
            } catch (Exception $e) {
                $app->render('koalacms/admin/generic/list.html', $_params);
            }
        });
        # ADD
        $app->post('', function($child_class) use ($app) {
            $_request = $app->request;
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_viewClass = "\KoalaCMS\View\\" . ucwords($child_class) . 'View';

            $_item = new $_class();
            $_view = new $_viewClass();

            $_view->handleSubmit($_item, $_request->params(), $_FILES);
            $_item->save();
            // Process Translation
            if($_item->translatable === true){
                $app->translator->proccess($_item, $_request->post());}

            $app->response->redirect($app->uri('/admin/' . $child_class));
        });
        # DETAIL
        $app->get('/:id', function($child_class, $id) use ($app) {
            $_request = $app->request;
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_viewClass = "\KoalaCMS\View\\" . ucwords($child_class) . 'View';

            $_item = call_user_func(array($_class, 'find'), $id);
            $_view = new $_viewClass();
            
            if($_item->translatable === true){
                $app->translator->load($_item);}

            $app->render('koalacms/admin/' . $child_class . '/detail.html', 
                array(
                    'item' => $_item,
                    'model' => $child_class,
                    'fields' => $_view->getFields()));
        });
        # UPDATE
        $app->post('/:id', function($child_class, $id) use ($app) {
            $_request = $app->request;
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_viewClass = "\KoalaCMS\View\\" . ucwords($child_class) . 'View';

            $_item = call_user_func(array($_class, "find"), $id);
            $_view = new $_viewClass();

            $_view->handleSubmit($_item, $_request->params(), $_FILES);
            $_item->save();
            // Process Translation
            if($_item->translatable === true){
                $app->translator->proccess($_item, $_request->post());}

            $app->response->redirect($app->uri('/admin/' . $child_class));
        });
        # DELETE
        $app->get('/:id/remove', function($child_class, $id) use ($app) {
            $_request = $app->request;
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
            $_item = call_user_func(array($_class, 'findOrFail'), $id);

            // Check for relations
            try {
                $_relatedClass = "\KoalaCMS\ContainerItem\\" . ucwords($child_class);
                $_related = call_user_func_array( array($_relatedClass, 'where'), array($child_class . '_id', '=', $_item->id))->firstOrFail();
                $_container = $_related->item();
                $_container->delete();
            } catch (Exception $e) {
                // Probably ModelNotFoundExceptionNotFoundException because it didnt found any relations...
            }

            try {
                $_item->delete();
            } catch (Exception $e) {
                // Probably already deleted somehow
            }
            
            $_url = $app->uri('/admin/' . $child_class);
            $app->response->redirect($_url);
        });
        # PUBLISH
        $app->get('/:id/:bool', function($child_class, $id, $bool) use ($app) {
            $_class = "\KoalaCMS\Model\\" . ucwords($child_class);

            $_item = call_user_func(array($_class, "find"), $id);
            $_item->published = ($bool == 'yes')? 1: 0;
            $_item->save();

            $_url = $app->uri('/admin/' . $child_class);
            $app->response->redirect($_url);
        });
    });
});

/*
 * Generic routes not enabled for now
# LIST
$app->get('/:child_class/', function($child_class) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
    $_items = call_user_func(array($_class, "OnlyPublished"))->get();
    // Intercep if language is defined and is not the default one
    handleTranslation($_items);
    echo $_items->toJson();
});
# DETAIL
$app->get('/:child_class/:slug', function($child_class, $slug) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_class = "\KoalaCMS\Model\\" . ucwords($child_class);
    $_item = call_user_func(array($_class, "bySlug"), $slug)->firstOrFail();
    handleTranslation($_item);
    echo $_item->toJson();
});
*/