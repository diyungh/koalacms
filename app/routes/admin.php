<?php

$app->get('/test', function() use ($app) {
    
    class M extends \Illuminate\Database\Eloquent\Model{}
    class P extends M{
        function items(){
            return $this->hasMany('');}
    }
    class CIImage extends M{}
    class CIAudio extends M{}
    class CIVideo extends M{}
    class Image extends M{}
    class Audio extends M{}
    class Video extends M{}

});

$app->get('/schema', function() use ($app) {
    $schema = $app->db->getSchemaBuilder();

    $schema->dropIfExists('album');
    $schema->dropIfExists('slot');
    $schema->dropIfExists('section');
    $schema->dropIfExists('project');
    $schema->dropIfExists('container_item_flatpage');
    $schema->dropIfExists('flatpage');
    $schema->dropIfExists('container_item_audio');
    $schema->dropIfExists('audio');
    $schema->dropIfExists('container_item_image');
    $schema->dropIfExists('image');    
    $schema->dropIfExists('container_item');
    $schema->dropIfExists('container_box');
    $schema->dropIfExists('translation');
    $schema->dropIfExists('language');

    $db = new \KoalaCMS\Model\Audio();
    $db->buildSchema();

    $db = new \KoalaCMS\ContainerItem\Audio();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Flatpage();
    $db->buildSchema();

    $db = new \KoalaCMS\ContainerItem\Flatpage();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Image();
    $db->buildSchema();

    $db = new \KoalaCMS\ContainerItem\Image();
    $db->buildSchema();

    $db = new \KoalaCMS\Core\ContainerBox();
    $db->buildSchema();

    $db = new \KoalaCMS\Core\ContainerItem();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Album();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Project();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Section();
    $db->buildSchema();

    $db = new \KoalaCMS\Model\Slot();
    $db->buildSchema();

    $db = new \Translate\Model\Language();
    $db->buildSchema();

    $db = new \Translate\Model\Translation();
    $db->buildSchema();

});