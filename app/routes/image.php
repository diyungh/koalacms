<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;

function noFilesError($name){
    $_error = $_FILES[$name]['error'];
    $_return = true;
    if(is_array($_error))
    {
        foreach($_error as $_fileError) 
            $_return &= ($_fileError === 0);
    }
    else if(!is_array($_error))
    {
        $_return &= ($_error === 0);
    }
    return $_return;}

function baseImagePath($fileName, $app, $file){
    // Get Tree
    $_basePath = $app->media . '/images' . date('/Y/m/d');
    $_baseFile = $_basePath . '/' . $fileName;
    // Path creation
    if (!file_exists($_basePath))
        mkdir($_basePath, 0777, true);
    // Get Extension
    $_fileExtension = explode('.', $file['name']);
    $_fileExtension = end($_fileExtension);
    //
    return $_baseFile . '.' . $_fileExtension;
}

function handleImage($fileName, $app, $file, &$_tmp, $_profile = null){    
    $filePath = baseImagePath($fileName, $app, $file);
    // Image Handling
    $_tmp = \Intervention\Image\Image::make($file['tmp_name']);

    if($_profile){ // Profile
        $_function = $_profile->action;
        $_tmp = call_user_func_array(array($_tmp, $_function), $_profile->params());
    }else if( !empty($_POST['resize-width']) ){ // Resize
        $_width = $_POST['resize-width']?: null;
        $_height = $_POST['resize-height']?: null;
        $_ratio = $_POST['resize-keep-proportion'] == 1? true: false;
        $_tmp = $_tmp->resize($_width, $_height, $_ratio);
    }else if( !empty($_POST['crop-width']) ){ // Crop
        $_width = $_POST['crop-width']?: null;
        $_height = $_POST['crop-height']?: null;
        $_offsetTop = $_POST['crop-offset-top']?: null;
        $_offsetLeft = $_POST['crop-offset-left']?: null;
        $_tmp = $_tmp->crop($_width, $_height, $_offsetLeft, $_offsetTop);
    }else if( !empty($_POST['grab-width']) ){ // Grab
        $_width = $_POST['grab-width']?: null;
        $_height = $_POST['grab-height']?: null;
        $_tmp = $_tmp->grab($_width, $_height);
    }
        
    $_tmp->save($filePath);
    return $filePath;
}

function calculatePages($total, $size = 10, $page = 1, $range = 3){
    if($total < $size)
        return null;
    if (isset($_GET['p']))
        $page = intval($_GET['p']);
    if($page < 1)
        $page = 1;
    $page--;
    return array(
        'pages' => ceil($total / $size),
        'current' => $page + 1,
        // Ranges and Helllip
        // TODO: MAKE HELLIP RANGES
        // Next|Prev Buttons
        'hasNext' => (($page + 1) * $size) < $total,
        'hasPrevious' => $page > 0,
        'next' => $page + 2,
        'previous' => $page);
}

function handleTranslation(&$_set){
    $app = \Koala\Slim::getInstance();

    if($languageCode = $app->request->get('lang')){
        if($languageCode !== $app->mainLanguage){
            $app->translator->translate($_set, $languageCode);
        }
    }
}

//TODO: Add behavior on presence of GET attribute _popup=1 to change template
// This way we can use raw id

$app->group('/admin', function() use ($app) {
    $app->group('/image', function() use ($app) {

        $app->get('', function() use ($app) {
            // Paginate calc
            $itemsPerPage = 10;
            $images = \KoalaCMS\Model\Image::onlyAvailable()->Ordered();
            // If Searching...
            if($app->request->get('q')){
                $_search = $app->request->get('q');
                $images = $images->Filter($_search);}
            // Paginate
            $_total = $images->count();
            $_results = $images->Paginated($itemsPerPage)->get();            
            // <admin>/<class-to-lower>/<action>
            $app->render('koalacms/admin/image/list.html', 
                array(
                    'profiles' => \KoalaCMS\Model\Imageprofile::all(),
                    'images' => $_results,
                    'paginate' => calculatePages($_total, $itemsPerPage)));
        });

        $app->post('', function() use ($app) {
            $_request = $app->request;
            $_img = new \KoalaCMS\Model\Image();

            // Translate
            $_img->title = $_request->post('title-' . $app->mainLanguage);
            $_img->description = $_request->post('description-' . $app->mainLanguage);
            $_img->published = isset($_POST['published'])?true:false;

            // Image Profile
            try {
                $_profile = \KoalaCMS\Model\Imageprofile::findOrFail($_request->post('profile'));
            } catch (Exception $e) {
                $_profile = null;}

            // Image Upload
            $_uploadKey = 'upload';
            $file = array(
                'name' => $_FILES[$_uploadKey]['name'],
                'tmp_name' => $_FILES[$_uploadKey]['tmp_name'],
                'error' => $_FILES[$_uploadKey]['error']);
            $_filename = $_img->slugify($_img->title . date('-h-i-s'));
            $img = null;
            $savePath = handleImage($_filename, $app, $file, $img, $_profile);
            $_img->width = $img->width;
            $_img->height = $img->height;
            $_img->file = $app->uri('/' . $savePath);

            // 
            $_img->save();
            // Save translations
            $app->translator->proccess($_img, $_request->post());
            $app->response->redirect($app->uri('/admin/image'));
        });

        $app->post('/upload', function() use ($app){

            $CKEditor = $_GET['CKEditor'] ;
            $funcNum = $_GET['CKEditorFuncNum'] ;
            $langCode = $_GET['langCode'] ;

            $_request = $app->request;
            $_img = new \KoalaCMS\Model\Image();

            $pattern = 'ckeditor-upload';
            // Translate
            $_img->title = $pattern;
            $_img->description = $pattern;

            // Image Profile
            try {
                $_profile = \KoalaCMS\Model\Imageprofile::findOrFail($_request->post('profile'));
            } catch (Exception $e) {
                $_profile = null;}

            // Image Upload
            $_uploadKey = 'upload';
            $file = array(
                'name' => $_FILES[$_uploadKey]['name'],
                'tmp_name' => $_FILES[$_uploadKey]['tmp_name'],
                'error' => $_FILES[$_uploadKey]['error']);
            $_filename = $_img->slugify($_img->title . date('-h-i-s'));
            $img = null;
            $savePath = handleImage($_filename, $app, $file, $img, $_profile);
            $_img->width = $img->width;
            $_img->height = $img->height;
            $_img->file = $app->uri('/' . $savePath);

            // 
            $_img->save();
            // Save translations
            $app->translator->proccess($_img, $_request->post());
            //CKEDITOR CALLBACK
            echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '$_img->file', 'Uploaded Successfully')</script>";
        });

        $app->get('/popup', function() use ($app) {
            // Paginate calc
            $itemsPerPage = 10;
            $images = \KoalaCMS\Model\Image::onlyAvailable()->OnlyPublished()->Ordered();
            // If Searching...
            if($app->request->get('q')){
                $images = $images->Filter($app->request->get('q'));}
            // Paginate
            $_total = $images->count();
            $_results = $images->Paginated($itemsPerPage)->get();
            // <admin>/<class-to-lower>/<action>
            $app->render('koalacms/admin/image/popup_list.html', 
                array(
                    'images' => $_results,
                    'paginate' => calculatePages($_total, $itemsPerPage)));
        });

        $app->get('/publish/:id/:bool', function($id, $bool) use ($app) {
            $_img = \KoalaCMS\Model\Image::findOrFail($id);
            $_img->published = ($bool == 'yes')? 1: 0;
            $_img->save();

            $app->response->redirect($app->uri('/admin/image'));
        });

        $app->get('/:id', function($id) use ($app) {
            $_img = \KoalaCMS\Model\Image::findOrFail($id);
            $app->translator->load($_img);
            $app->render('koalacms/admin/image/detail.html', array('image' => $_img));
        });

        $app->post('/:id', function($id) use ($app) {
            $_request = $app->request;
            $_img = \KoalaCMS\Model\Image::findOrFail($id);

            $_img->title = $_request->post('title-' . $app->mainLanguage);
            $_img->description = $_request->post('description-' . $app->mainLanguage);
            $_img->published = isset($_POST['published'])?true:false;

            // Change image
            if($_FILES['upload']['error'] === 0){
                $_uploadKey = 'upload';

                $file = array(
                    'name' => $_FILES[$_uploadKey]['name'],
                    'tmp_name' => $_FILES[$_uploadKey]['tmp_name'],
                    'error' => $_FILES[$_uploadKey]['error']);
                $_filename = $_img->slugify($_img->title);
                $img = null;
                $savePath = handleImage($_filename, $app, $file, $img);
                $_img->width = $img->width;
                $_img->height = $img->height;
                $_img->file = $app->uri('/' . $savePath);
            }
            $_img->save();
            $app->translator->proccess($_img, $_request->post());

            $app->response->redirect($app->uri('/admin/image'));
        });

        $app->get('/remove/:id', function($id) use ($app) {
            $_img = \KoalaCMS\Model\Image::findOrFail($id);

            // Check for relations
            try {
                $_related = \KoalaCMS\ContainerItem\Image::where('image_id', '=', $_img->id)->firstOrFail();
                $_item = $_related->item();
                $_item->delete();
            } catch (Exception $e) {
                // Probably ModelNotFoundExceptionNotFoundException because it didnt found any relations...
            }

            $_img->delete();
            $app->response->redirect($app->uri('/admin/image'));
        });

    });
});

$app->get('/image', function() use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_images = \KoalaCMS\Model\Image::OnlyPublished()->get();
    // Intercep if language is defined and is not the default one
    handleTranslation($_images);
    echo $_images->toJson();
});

$app->get('/image/:slug', function($slug) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $image = \KoalaCMS\Model\Image::bySlug($slug)->firstOrFail();
    handleTranslation($image);
    
    echo $image->toJson();
});
