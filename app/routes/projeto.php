<?php
use \Illuminate\Database\Eloquent\ModelNotFoundException;

$app->group('/admin', function() use ($app) {

    // LIST
    $app->get('/project', function() use ($app) {

        $itemsPerPage = 10;
        $items = \KoalaCMS\Model\Project::onlyAvailable()->Ordered();
        
        if($app->request->get('q')){
            $_search = $app->request->get('q');
            $items = $items->Filter($_search);}

        $_total = $items->count();
        $_results = $items->Paginated($itemsPerPage)->get();            

        $app->render('koalacms/admin/project/list.html', array(
            'projects' => $_results,
            'paginate' => calculatePages($_total, $itemsPerPage)
        ));
    });

    // ADD
    $app->post('/project', function() use ($app) {
        $project = new \KoalaCMS\Model\Project;
        
        // Translate
        $project->title = $_POST['title-' . $app->mainLanguage];
        $project->description = $_POST['description-' . $app->mainLanguage];
        $project->published = isset($_POST['published'])?true:false;

        $project->save();

        //Save Translations
        $app->translator->proccess($project, $app->request->post());

        if(empty($_FILES)){
            try {
                // Dont have image check for raw ids
                foreach($_POST['picture-raw-id'] as $key => $_relatedPicture){
                    $_img = \KoalaCMS\Model\Image::find($_relatedPicture);
                    $project->addItem($_img, $_POST['picture-raw-order'][$key]?:'0');
                }
            } catch (ErrorException $e){
                // no images related too.
            }
        }
        elseif(noFilesError('picture-file')){
            // Have image upload them all!
            // We have images...
            foreach($_POST['picture-title'] as $key => $imageTitle){    
                $_img = new \KoalaCMS\Model\Image;
                $_img->title = $_POST['picture-title'][$key];
                $_img->description = $_POST['picture-description'][$key];
                $_uploadKey = 'picture-file';

                $file = array(
                    'name' => $_FILES[$_uploadKey]['name'][$key],
                    'tmp_name' => $_FILES[$_uploadKey]['tmp_name'][$key],
                    'error' => $_FILES[$_uploadKey]['error'][$key]);

                $_filename = $_img->slugify($_img->title);
                $img = null;
                $savePath = handleImage($_filename, $app, $file, $img);

                $_img->width = $img->width;
                $_img->height = $img->height;
                $_img->file = $app->uri('/' . $savePath);
                $_img->save();

                $project->addItem($_img, $_POST['picture-order'][$key]?:'0');
            }
        };
        $project->push();

        $url = $app->uri('/admin/project/' . $project->id);
        $app->response->redirect($url);
    });

    $app->get('/project/:id/publish/:bool', function($id, $bool) use ($app) {
        $project = \KoalaCMS\Model\Project::where('id', '=', $id)->firstOrFail();
        $project->published = ($bool == 'yes')? 1: 0;
        $project->save();

        $app->response->redirect($app->uri('/admin/project'));
    });

    //DETAIL
    $app->get('/project/:id', function($id) use ($app) {
        try {
            $project = call_user_func(array("\KoalaCMS\Model\Project", "findOrFail"), $id);
            $app->translator->load($project);
        } catch (ModelNotFoundException $e) {
            $project = null;
        }
        $app->render('koalacms/admin/project/detail.html', array('project' => $project));
    });

    //DELETE
    $app->get('/project/:id/delete', function($id) use ($app) {

        $_status = 200;
        $_url = $app->uri('/admin/project');

        try {
            call_user_func(array("\KoalaCMS\Model\Project", "findOrFail"), $id)->delete();
        } catch (Exception $e) {
            var_dump($e);
            $_status = 400;
        }
        
        $app->response->redirect($_url);
    });

    // UPDATE
    $app->post('/project/:id', function($id) use ($app) {
        $project = \KoalaCMS\Model\Project::find($id);
        var_dump($_POST);
        $project->title = $_POST['title-' . $app->mainLanguage];
        $project->description = $_POST['description-' . $app->mainLanguage];
        $project->published = isset($_POST['published'])? true: false;
        $project->save();
        //Save Translations
        $app->translator->proccess($project, $app->request->post());

        // Files

        if(empty($_FILES)){
            // Try to newly related images
            try {
                // Dont have image check for raw ids
                foreach($_POST['picture-raw-id'] as $key => $_relatedPicture){
                    $_img = \KoalaCMS\Model\Image::find($_relatedPicture);
                    $project->addItem($_img, $_POST['picture-raw-order'][$key]?:'0');
                }
            } catch (ErrorException $e){}

            // Try to order changed
            try {
                // Dont have image check for raw ids
                foreach($_POST['container-order'] as $key => $_order){
                    $_id = $_POST['container-id'][$key];
                    $_container = \KoalaCMS\Core\ContainerItem::find($_id);
                    $_container->order = $_order;
                    $_container->save();
                }
            } catch (ErrorException $e){}

            // Try to remove
            try {
                // Dont have image check for raw ids
                foreach($_POST['container-delete'] as $key => $_delete){
                    if($_delete === '0')
                        continue;
                    $_container = \KoalaCMS\Core\ContainerItem::find($_delete);
                    $_container->delete();
                }
            } catch (ErrorException $e){}
        }
        elseif(noFilesError('picture-file')){
            // Have image upload them all!
            // We have images...
            foreach($_POST['picture-title'] as $key => $imageTitle){    
                $_img = new \KoalaCMS\Model\Image;
                $_img->title = $_POST['picture-title'][$key];
                $_img->description = $_POST['picture-description'][$key];
                $_uploadKey = 'picture-file';

                $file = array(
                    'name' => $_FILES[$_uploadKey]['name'][$key],
                    'tmp_name' => $_FILES[$_uploadKey]['tmp_name'][$key],
                    'error' => $_FILES[$_uploadKey]['error'][$key]);

                $_filename = $_img->slugify($_img->title);
                $img = null;
                $savePath = handleImage($_filename, $app, $file, $img);
                
                $_img->width = $img->width;
                $_img->height = $img->height;
                $_img->file = $app->uri('/' . $savePath);
                $_img->save();

                $project->addItem($_img, $_POST['picture-order'][$key]?:'0');
            }
        };
        $project->push();

        //

        $url = $app->uri('/admin/project/' . $project->id);
        $app->response->redirect($url, 200);
    });

});

# LIST
$app->get('/project/', function() use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_class = "\KoalaCMS\Model\Project";
    $_items = $_class::OnlyPublished()->get();
    // Intercep if language is defined and is not the default one
    handleTranslation($_items);
    echo $_items->toJson();
});
# DETAIL
$app->get('/project/:slug', function($slug) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_class = "\KoalaCMS\Model\Project";
    $_item = $_class::bySlug($slug)->firstOrFail();
    handleTranslation($_item);
    echo $_item->toJson();
});