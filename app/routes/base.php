<?php

use KoalaCMS\Model\Section;
use KoalaCMS\Model\Slot;
use KoalaCMS\Model\ProductSlot;

$app->group('/admin', function() use ($app) {
    $app->get('', function() use ($app) {
        $app->render('koalacms/base.html');
    });

    $app->get('/feature', function() use ($app) {

        $mySection = new Section();
        $mySection->title = 'test_section';
        $mySection->save();

        $myProduct = new ProductSlot();
        $myProduct->title = 'produto 1';
        $myProduct->slug = 'produto-1';
        $myProduct->description = 'descricao do produto 1';
        $myProduct->save();

        $mySlotA = new Slot();
        $mySlotA->published = TRUE;
        $mySlotA->order = 1;

        $mySection->add($mySlotA);
        $mySlotA->bind($myProduct);
        $mySection->push();

        var_dump('hell yeah!');
    });

    $app->get('/feature/update', function() use ($app){
        $mySection = Section::find(14);
        var_dump($mySection->slots);
    });
});