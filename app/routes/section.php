<?php

use \Illuminate\Database\Eloquent\ModelNotFoundException;
use \KoalaCMS\Model\Section;
use \KoalaCMS\Model\Image;

$app->group('/admin', function() use ($app) {
    $app->group('/section', function() use ($app) {
        
        $app->get('', function() use ($app) {
            $app->render('koalacms/admin/section/list.html', 
                array('sections' => Section::all())
            );
        });

        $app->post('/:section/:slot', function($section, $slot) use ($app){
            $_section = Section::find($section);
            $_slot = $_section->slots()->where('id', '=', $slot)->get()->first();

            $_raw_id = $_POST["item-raw-id"];
            $_imageon_raw_id = $_POST['imageon-raw-id'];
            $_imageoff_raw_id = $_POST['imageoff-raw-id'];
            $_child_class = $_POST['item-child-class'];
            $_item_delete = $_POST['item-delete'];

            $data = array('status' => 200, 'data' => null, 'message' => '');

            if(!empty( $_item_delete ))
            {
                $_slot->clear();
                $data['message'] = 'Slot apagado.';
                $data['data'] = array(
                    'action' => 'delete',
                    'slot_id' => $_slot->id);
            }
            else if(empty( $_item_delete ))
            {
                $_slot->image_on_id = $_imageon_raw_id;
                $_slot->image_off_id = $_imageoff_raw_id;
                $_slot->container_id = $_raw_id;
                $_slot->container_type = "\KoalaCMS\Model\\" . $_child_class;

                $_image_on = Image::findOrFail($_imageon_raw_id);
                $_image_off = Image::findOrFail($_imageoff_raw_id); 
                $_container = call_user_func(array($_slot->container_type, 'findOrFail'), $_slot->container_id);
                $data['data'] = array(
                    'action' => 'update',
                    'slot_id' => $_slot->id,
                    'container_type' => $_child_class,
                    'container' => $_container->title,
                    'image_on' => $_image_on->file,
                    'image_off' => $_image_off->file);
                $data['message'] = 'Slot atualizado.';
            }

            $_slot->save();
            $_section->save();
            echo json_encode($data);
        });

        $app->post('/:id', function($id) use ($app) {
            $_section = Section::find($id);
            $_slots = $_section->slots()->Ordered('order', 'ASC')->get();
            $data = array();
            // We should look for slots
            $_raw_id = $_POST["item-raw-id"];
            foreach($_raw_id as $key => $value){
                $_slot = $_slots[$key];
                $_item_delete = $_POST['item-delete'];
                if(!empty( $_item_delete[$key] )){
                    $_slot->clear();
                }elseif(empty( $_raw_id[$key] )){
                    continue;
                }else{
                    $_imageon_raw_id = $_POST['imageon-raw-id'];
                    $_imageoff_raw_id = $_POST['imageoff-raw-id'];
                    $_child_class = $_POST['item-child-class'];

                    $_slot->image_on_id = $_imageon_raw_id[$key];
                    $_slot->image_off_id = $_imageoff_raw_id[$key];
                    $_slot->container_id = $_raw_id[$key];
                    $_slot->container_type = "\KoalaCMS\Model\\" . $_child_class[$key];

                    $_image_on = Image::findOrFail($_imageon_raw_id[$key]);
                    $_image_off = Image::findOrFail($_imageoff_raw_id[$key]);
                    $data[] = array(
                        'slot_id' => $_slot->id,
                        'image_on' => $_image_on->file,
                        'image_off' => $_image_off->file);
                }
                $_slot->save();
            }
            $_section->save();
            //$app->response->redirect($app->uri('/admin/section'));
            $app->response->headers->set('Content-Type', 'application/json');
            echo '{"status": 200, "data": ' . json_encode($data) . ', "message": "Slots updated successfully"}';
        });

        $app->get('/popup', function() use ($app) {
            // Paginate calc
            $itemsPerPage = 10;
            $flatpages = \KoalaCMS\Model\Flatpage::onlyAvailable()->OnlyPublished()->Ordered();
            $projects = \KoalaCMS\Model\Project::onlyAvailable()->OnlyPublished()->Ordered();
            $links = \KoalaCMS\Model\Link::onlyAvailable()->OnlyPublished()->Ordered();
            // If Searching...
            if($app->request->get('q')){
                $flatpages = $flatpages->Filter($app->request->get('q'));
                $projects = $projects->Filter($app->request->get('q'));
                $links = $links->Filter($app->request->get('q'));
            }

            $_projects = $projects->get()->toArray();
            $_flatpages = $flatpages->get()->toArray();
            $_links = $links->get()->toArray();
            $app->render('koalacms/admin/section/popup_list.html', 
                array('items' => array_merge($_projects, $_flatpages, $_links)));
        });

    });
});

$app->get('/section', function() use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    
    echo Section::OnlyPublished()->get()->toJson();
});

$app->get('/section/:id', function($id) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');

    $_section = Section::find($id);
    handleTranslation($_section);
    echo $_section->toJson();
    $_slots = $_section->slots()->OnlyPublished()->Ordered('order', 'ASC')->get();
    echo $_slots->toJson();
});