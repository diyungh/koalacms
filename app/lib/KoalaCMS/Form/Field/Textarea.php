<?php

namespace KoalaCMS\Form\Field;

class Textarea{
    public function input($name = '', $value = ''){
        return "<textarea name='$name'>$value</textarea>";
    }
}