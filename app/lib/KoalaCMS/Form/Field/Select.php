<?php

namespace KoalaCMS\Form\Field;

class Select{
    public $onMiss = '0';
    public function input($name = '', $options = array(), $index = null){
        $_el = "<select name='$name'>";
        foreach($options as $key=>$value){
            $_checked = '';
            if($key === $index)
                $_checked = 'CHECKED';
            $_el .= "<option value='$key' $_checked>$value</option>";}
        $_el .= "</select>";
        return $_el;
    }
    public function read($text){
        return $text;
    }
}