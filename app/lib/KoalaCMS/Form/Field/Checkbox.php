<?php

namespace KoalaCMS\Form\Field;

class Checkbox{
    public $onMiss = '0';
    public function input($name = '', $checked = ''){
        $_checked = $checked == '1'? 'CHECKED': '';
        return "<input type='checkbox' name='$name' value='1' $_checked/>";
    }
    public function read($checked, $url = null){
        $_checked = $checked == '1'? 'yes': 'no';
        $_str = "<div class='published $_checked'></div>";
        if($url)
            $_str = "<a href='$url'>$_str</a>";
        return $_str;
    }
}