<?php

namespace KoalaCMS\View;

use KoalaCMS\Model\Flatpage;

class LinkView extends \KoalaCMS\View\View{
    public $listOrder = array('title', 'url', 'published', 'external');
    public $fields = array(

        'title' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Título',
            'name' => 'title',
            'col' => 6,
            'order' => 0,
            'translate' => true),

        'description' => array(
            'type' => '\CKEditor\Form\Field\Textarea',
            'label' => 'Descrição',
            'name' => 'description',
            'col' => 12,
            'order' => 5,
            'translate' => true),

        'url' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'URL',
            'name' => 'url',
            'col' => 6,
            'order' => 1,
            'translate' => true),

        'external' => array(
            'type' => '\KoalaCMS\Form\Field\Checkbox',
            'label' => 'Externo',
            'name' => 'external',
            'col' => 2,
            'order' => 3,
            'translate' => false),

        'published' => array(
            'type' => '\KoalaCMS\Form\Field\Checkbox',
            'label' => 'Publicado',
            'name' => 'published',
            'col' => 2,
            'order' => 2,
            'translate' => false)
    );
}