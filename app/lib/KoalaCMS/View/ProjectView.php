<?php

namespace KoalaCMS\View;

use KoalaCMS\Model\Project;

class ProjectView extends \KoalaCMS\View\View{
    public $fields = array(
        'title' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Título',
            'name' => 'title',
            'col' => 4),
        'description' => array(
            'type' => '\CKEditor\Form\Field\Textarea',
            'label' => 'Descrição',
            'name' => 'description',
            'col' => 12)
    );
}