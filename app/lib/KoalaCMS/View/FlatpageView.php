<?php

namespace KoalaCMS\View;

use KoalaCMS\Model\Flatpage;

class FlatpageView extends \KoalaCMS\View\View{
    public $listOrder = array('title', 'description', 'published');
    public $fields = array(

        'title' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Título',
            'name' => 'title',
            'col' => 6,
            'order' => 0,
            'translate' => true),

        'description' => array(
            'type' => '\CKEditor\Form\Field\Textarea',
            'label' => 'Descrição',
            'name' => 'description',
            'col' => 12,
            'order' => 2,
            'translate' => true),

        'published' => array(
            'type' => '\KoalaCMS\Form\Field\Checkbox',
            'label' => 'Publicado',
            'name' => 'published',
            'col' => 3,
            'order' => 1,
            'translate' => false)
    );
}