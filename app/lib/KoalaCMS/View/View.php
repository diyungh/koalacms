<?php

namespace KoalaCMS\View;


class View{

    private $app = null;

    public function __construct(){
        $this->app = call_user_func(array("\Koala\Slim", 'getInstance'));
    }

    public $fields = array();


    public function proccessField($field){
        $_processor = new $field['type']();
        return $_processor;
    }

    public function getFields($sorted = false){
        $_fields = array();
        foreach($this->fields as $key => $field){
            $_field = $this->proccessField($field);
            if($_field !== null){
                $field['processor'] = $_field;
                $_fields[$key] =  $field;
            }
        }
        // Order
        if($sorted === true)
            uasort($_fields, function($a, $b){return intval($a['order']) - intval($b['order']);});
        //
        return $_fields;
    }

    public function handleSubmit($modelInstance, $params, $files){
        foreach($this->getFields() as $field){
            $_fieldName = $field['name'];
            $_attr = $field['name'];

            if($field['translate'] === true){
                $_attr = $_attr . '-' . $this->app->mainLanguage;}

            if(isset($params[$_attr]))
                $modelInstance->$_fieldName = $params[$_attr];
            else if(!isset($params[$_attr])){
                $modelInstance->$_fieldName = $field['processor']->onMiss;}
        }
        return $modelInstance;
    }
}