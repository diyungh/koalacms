<?php

namespace KoalaCMS\View;

use KoalaCMS\Model\Section;

class SectionView extends \KoalaCMS\View\View{
    public $fields = array(
        'title' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Título',
            'name' => 'title',
            'col' => 4)
    );
}