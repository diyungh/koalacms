<?php

namespace KoalaCMS\View;

use KoalaCMS\Model\Imageprofile;

class ImageprofileView extends \KoalaCMS\View\View{
    public $listOrder = array('title', 'description', 'published');
    public $fields = array(

        'action' => array(
            'type' => '\KoalaCMS\Form\Field\Select',
            'label' => 'Ação',
            'name' => 'action',
            'col' => 12,
            'order' => 0,
            'translate' => false),

        // Grab
        'grab_width' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Width',
            'name' => 'grab_width',
            'col' => 3,
            'order' => 1,
            'translate' => false),

        'grab_height' => array(
            'type' => '\KoalaCMS\Form\Field\Text',
            'label' => 'Height',
            'name' => 'grab_height',
            'col' => 3,
            'order' => 1,
            'translate' => false)
    );
}