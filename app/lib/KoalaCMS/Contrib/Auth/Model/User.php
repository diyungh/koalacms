<?php

namespace Auth\Model;

class User extends \KoalaCMS\Core\Model{
    protected $table = 'user';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->string('name');
        $table->string('username');
        $table->string('password');
    }
    
}