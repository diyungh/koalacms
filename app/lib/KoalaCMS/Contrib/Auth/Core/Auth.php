<?php

namespace Auth\Core;

class Auth{

    protected $realm = 'Protected Area';

    public static function authenticate($user, $pass){
        try {
            $_pass = md5($pass);
            \Auth\Model\User::where('username', '=', $user)
            ->where('password', '=', $_pass)->firstOrFail();
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    public static deny(){
        $app = \Koala\Slim::getInstace();
        $res = $app->response();
        $res->status(401);
        $res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $self::realm));
    }

    public static function loggedin(){
        $app = \Koala\Slim::getInstace();
        $req = $app->request();
        $res = $app->response();
        $authUser = $req->headers('PHP_AUTH_USER');
        $authPass = $req->headers('PHP_AUTH_PW');
         
        return $this->authenticate($authUser, $authPass);
    }
}