<?php

namespace Translate\Model;

class Language extends \KoalaCMS\Core\Model{
    protected $table = 'language';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->string('title')->nullable();
        $table->string('code')->nullable();
    }
}