<?php

namespace Translate\Model;

class Translation extends \KoalaCMS\Core\Model{
    protected $table = 'translation';

    public function scopeByObject($query, $object, $languageId){
        return $query
            ->where('container_type', '=', $object->getClass())
            ->where('container_id', '=', $object->id)
            ->where('language_id', '=', $languageId);
    }

    public function getSchema(&$table){
        parent::getSchema($table);
        # Identifier
        $table->string('slug');

        # Morph
        $table->string('container_type')->nullable();
        $table->integer('container_id')->unsigned()->nullable();

        # Language
        $table->integer('language_id')->unsigned()->nullable();
        $table->foreign('language_id')->references('id')->on('language');

        # Equivalent to replace
        $table->text('equivalent')->nullable();
    }

}