<?php

namespace Translate\Core;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Collection;

class ModelTranslator{

    public function __construct($db){
        $this->app = call_user_func(array("\Koala\Slim", "getInstance"));
        $this->db = $this->app->db;}

    private function proccessField($field, $modelInstance, $vars){
        $_keys = array_keys($vars);
        $filteredKeys = array_filter($_keys, function($item) use ($field) {return strpos($item, $field) === 0;});
        // for each translation that came we process it
        foreach ($filteredKeys as $languageKey) {
            $_value = $vars[$languageKey];
            $_fieldName = substr($languageKey, 0, -6);
            $_languageCode = substr($languageKey, -5);
            $_language = $this->getLanguageByCode($_languageCode);
            // save
            $this->setTranslatedField($_language->id, $modelInstance->getClass(), $modelInstance->id, $_fieldName, $_value);
        }
    }

    private function translateModel(&$item, $_language){
        $_translations = call_user_func_array(array("\Translate\Model\Translation", "byObject"), array($item, $_language->id))->get();
        foreach($_translations as $_translation){
            $_field = $_translation->slug;
            $item->$_field = $_translation->equivalent;
        }
    }

    private function loadField($_fieldName, &$modelInstance){
        $_languages = $this->getLanguages();
        // for each translation that came we process it
        foreach ($_languages as $_language) {
            try {
                // load
                $_translation = $this->getTranslatedField($_language->id, $modelInstance->getClass(), $modelInstance->id, $_fieldName);
                // append
                $_attr = $_fieldName . '-' . $_language->code;
                $modelInstance->$_attr = $_translation->equivalent;
            } catch (ModelNotFoundException $e) {
                $_attr = $_fieldName . '-' . $_language->code;
                $_value = '';

                if($_language->code == $this->app->mainLanguage)
                    $_value = $modelInstance->$_fieldName;
                $modelInstance->$_attr = $_value;
            }
        }
    }

    public function translate(&$items, $languageCode){
        $_language = $this->getLanguageByCode($languageCode);

        // Translate Once and return
        if(!$items instanceOf Collection && !is_array($items)){
            $this->translateModel($items, $_language);
            return;}

        // Translate set
        foreach($items as $item){
            // Not translatable
            if($item->translatable !== true)
                continue;
            // No fields to translate
            if(sizeof($item->translatable_fields) <= 0)
                continue;
            // Translate
            $this->translateModel($item, $_language);
        }
    }

    public function load(&$modelInstance){
        // For each field that need to be translated
        foreach ($modelInstance->translatable_fields as $key => $field) {
            $this->loadField($field, $modelInstance);
        }
    }

    public function proccess($modelInstance, $vars){
        // For each field that need to be translated
        foreach ($modelInstance->translatable_fields as $key => $field) {
            $this->proccessField($field, $modelInstance, $vars);
        }
    }

    public function getLanguageByCode($code){
        return call_user_func_array(array("\Translate\Model\Language", "where"), array('code', '=', $code))->firstOrFail();
    }
    
    public function getLanguages(){
        return call_user_func(array("\Translate\Model\Language", "all"));
    }

    public function getTranslatedField($languageId, $instanceType, $instanceID, $fieldName){
        return call_user_func_array(array("\Translate\Model\Translation", "where"), array('slug', '=', $fieldName))
            ->where('container_type', '=', $instanceType)
            ->where('container_id', '=', $instanceID)
            ->where('language_id', '=', $languageId)
            ->firstOrFail();
    }

    private function _createTranslation($languageId, $instanceType, $instanceID, $fieldName, $fieldValue){
        $_translation = new \Translate\Model\Translation();
        $_translation->container_type = $instanceType;
        $_translation->container_id = $instanceID;
        $_translation->slug = $fieldName;
        $_translation->equivalent = $fieldValue;
        $_translation->language_id = $languageId;
        return $_translation->save();
    }

    public function setTranslatedField($languageId, $instanceType, $instanceID, $fieldName, $fieldValue){
        try{
            $_translation = call_user_func_array(array("\Translate\Model\Translation", "where"), array('slug', '=', $fieldName))
                ->where('container_type', '=', $instanceType)
                ->where('container_id', '=', $instanceID)
                ->where('language_id', '=', $languageId)
                ->firstOrFail();
            $_translation->equivalent = $fieldValue;
            $_translation->save();
        }catch(ModelNotFoundException $ex){
            $this->_createTranslation($languageId, $instanceType, $instanceID, $fieldName, $fieldValue);
        }
    }
}