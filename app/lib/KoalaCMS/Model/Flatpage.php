<?php

namespace KoalaCMS\Model;

class Flatpage extends \KoalaCMS\Core\Container{
    public $table = 'flatpage';
    public $translatable = true;
    public $translatable_fields = array('title', 'description');
}