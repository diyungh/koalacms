<?php

namespace KoalaCMS\Model;

class Image extends \KoalaCMS\Core\Cropable{
    public $table = 'image';
    public $translatable = true;
    public $translatable_fields = array('title', 'description');

    public function getSchema(&$table){
        parent::getSchema($table);
    }

    //TODO: ALTER VENDOR TO IMPLEMENTE MEDIUMBLOB LIKE ->binary(fieldName, blobSize)
    // WHERE blobSize is enum of: TINYBLOB, BLOB, MEDIUMBLOB, LONGBLOB

    //TODO: FileUpload Interface, field Upload($to=resolver(Image, fileName))

    public function url(){}
}