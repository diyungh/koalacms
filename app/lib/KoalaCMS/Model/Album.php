<?php

namespace KoalaCMS\Model;

class Album extends \KoalaCMS\Core\Boxable{
    public $table = 'album';
    public $recursive = array('images');

    public function getSchema(&$table){
        parent::getSchema($table);
    }

    public function images(){
        return $this->items();
    }
}