<?php

namespace KoalaCMS\Model;

class Tag extends \KoalaCMS\Core\Container{
    public $table = 'tag';
    public $translatable = true;
    public $translatable_fields = array('title');
}