<?php

namespace KoalaCMS\Model;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class Slot extends \KoalaCMS\Core\Publishable{
    public $table = 'slot';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->removeColumn('published')
              ->boolean('published')->default(false);
        # Parent Section
        $table->integer('section_id')->unsigned();
        $table->foreign('section_id')->references('id')->on('section');
        # Slot Order
        $table->integer('order')->unsigned()->default(0);
        # Item Morph
        $table->integer('container_id')->unsigned()->nullable();
        $table->string('container_type')->nullable();
    }

    public function container(){
        return $this->morphTo();
    }

    // Should be a polymorphic morph
    public function _item(){
        if(!isset($this->container_type) || !isset($this->container_id))
            return false;
        try {
            $_model = $this->container_type;
            return $_model::where('id', '=', $this->container_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    public function bind($Object){
        $Object->slot()->save($this);
    }

}