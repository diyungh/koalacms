<?php

namespace KoalaCMS\Model;

class Section extends \KoalaCMS\Core\Container{
    public $table = 'section';
    public $translatable = true;
    public $translatable_fields = array('title');

    public function getSchema(&$table){
        parent::getSchema($table);
    }

    public function slots(){
        return $this->hasMany('\KoalaCMS\Model\Slot');
    }

    public function add($Object){
        $this->slots()->save($Object);
    }

}