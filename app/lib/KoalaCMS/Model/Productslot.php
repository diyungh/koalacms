<?php

namespace KoalaCMS\Model;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductSlot extends \KoalaCMS\Core\SlotItem{
    public $table = 'product_slot';

    public function getSchema(&$table){
        parent::getSchema($table);
        # Slot Order
        $table->string('title');
        $table->string('slug');
        $table->text('description')->nullable();
    }
}