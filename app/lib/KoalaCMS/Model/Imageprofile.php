<?php

namespace KoalaCMS\Model;

class ImageProfile extends \KoalaCMS\Core\Container{
    public $table = 'image_profile';
    public $translatable = false;
    public $translatable_fields = array();

    public function params(){
        switch($this->action){
            case 'grab':
                return array($this->grab_width, $this->grab_height);
            case 'resize':
                return array($this->resize_width, $this->resize_height,
                             $this->resize_keep_proportion);
            case 'crop':
                return array($this->crop_width, $this->crop_height,
                             $this->crop_offset_top, $this->crop_offset_left,
                             $this->crop_center_crop);
        }
        return array();
    }

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->enum('action', array('grab', 'resize', 'crop'));
        // Grab
        $table->string('grab_width', 7)->nullable();
        $table->string('grab_height', 7)->nullable();
        // Resize
        $table->string('resize_width', 7)->nullable();
        $table->string('resize_height', 7)->nullable();
        $table->boolean('resize_keep_proportion')->default(False);
        // Crop
        $table->string('crop_width', 7)->nullable();
        $table->string('crop_height', 7)->nullable();
        $table->string('crop_offset_top', 7)->nullable();
        $table->string('crop_offset_left', 7)->nullable();
        $table->boolean('crop_center_crop')->default(False);
    }

}