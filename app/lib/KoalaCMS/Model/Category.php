<?php

namespace KoalaCMS\Model;

class Category extends \KoalaCMS\Core\Container{
    public $table = 'category';
    public $translatable = true;
    public $translatable_fields = array('title');

    public function parent(){
        return $this->belongsToMany('KoalaCMS\Model\Category');
    }

    public function children(){
        return $this->hasMany('KoalaCMS\Model\Category');
    }
}