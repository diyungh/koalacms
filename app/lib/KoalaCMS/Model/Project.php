<?php

namespace KoalaCMS\Model;

class Project extends \KoalaCMS\Model\Album{
    public $table = 'project';
    public $translatable = true;
    public $translatable_fields = array('title', 'description');
}