<?php

namespace KoalaCMS\Model;

class Link extends \KoalaCMS\Core\Container{
    public $table = 'link';
    public $translatable = true;
    public $translatable_fields = array('title', 'description', 'url');

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->string('url')->nullable();
        $table->boolean('external')->default(True);
    }
}