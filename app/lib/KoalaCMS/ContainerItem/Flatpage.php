<?php

namespace KoalaCMS\ContainerItem;

class Flatpage extends \KoalaCMS\Core\Model{
    protected $table = 'container_item_flatpage';
    public $translatable_fields = array('title', 'description');

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('flatpage_id')->unsigned();
        $table->foreign('flatpage_id')->references('id')->on('flatpage');
    }

    public function flatpage(){
        return $this->belongsTo('\KoalaCMS\Model\Flatpage')->firstOrFail();
    }

    public function item(){
        return $this->morphOne('\KoalaCMS\Core\ContainerItem', 'container');
    }
}