<?php

namespace KoalaCMS\ContainerItem;

class Image extends \KoalaCMS\Core\Model{
    protected $table = 'container_item_image';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('image_id')->unsigned();
        $table->foreign('image_id')->references('id')->on('image');
    }

    public function image(){
        return $this->belongsTo('\KoalaCMS\Model\Image')->firstOrFail();
    }

    public function item(){
        return $this->morphOne('\KoalaCMS\Core\ContainerItem', 'container')->firstOrFail();
    }
}