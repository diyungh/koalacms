<?php

namespace KoalaCMS\ContainerItem;

class Audio extends \KoalaCMS\Core\Model{
    protected $table = 'container_item_audio';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('audio_id')->unsigned();
        $table->foreign('audio_id')->references('id')->on('audio');
    }

    public function audio(){
        return $this->belongsTo('\KoalaCMS\Model\Audio')->firstOrFail();
    }

    public function item(){
        return $this->morphOne('\KoalaCMS\Core\ContainerItem', 'container');
    }
}