<?php

namespace KoalaCMS\Core;

class Resizeable extends \KoalaCMS\Core\Archive{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('width')->unsigned()->nullable();
        $table->integer('height')->unsigned()->nullable();
        $table->boolean('ratio')->default(true)->nullable();
    }

}