<?php

namespace KoalaCMS\Core;

class ContainerBox extends \KoalaCMS\Core\Model{
    protected $table = 'container_box';

    public function getSchema(&$table){
        parent::getSchema($table);
    }

    public function addItem($item, $order){
        $_item = new \KoalaCMS\Core\ContainerItem;
        $_item->order = $order;
        $_item->setItem($item);
        $_item->container_box_id = $this->id;
        $_item->save();
        // FIXME: We should mount a cascade SAVE not save it everytime you call for add
    }

    public function removeItem($item){
        $this->_containers()->where('id', '=', $item->id)->firstOrFail()->delete();
    }

    public function _containers(){
        return $this->hasMany('\KoalaCMS\Core\ContainerItem')->ordered();
    }

    public function containers(){
        $_return = array();
        $_containers = $this->_containers();
        $_containerItems = $_containers->getResults();

        foreach($_containerItems as $item){
            $_item = $item->container()->first();
            $_return[] = $_item;
        }
        return $_return;
    }

    public function delete()
    {
        foreach($this->_containers()->getResults() as $_containerItem)
            $_containerItem->delete();
        parent::delete();
    }
}