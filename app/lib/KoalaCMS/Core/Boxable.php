<?php

namespace KoalaCMS\Core;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class Boxable extends \KoalaCMS\Core\Container{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('containerbox_id')->unsigned()->nullable();
        $table->foreign('containerbox_id')->references('id')->on('container_box');
    }

    public function containerbox(){
        return $this->belongsTo('\KoalaCMS\Core\ContainerBox')->firstOrFail();
    }

    public function createContainerBox(){
        $containerBox = new \KoalaCMS\Core\ContainerBox;
        $containerBox->save();
        $this->containerbox_id = $containerBox->id;
    }

    public function items(){
        try {
            return $this->containerbox()->containers();
        } catch (ModelNotFoundException $e) {
            return array();
        }
    }

    public function switchItem(){}

    public function addItem($item, $order = 0){
        try {
            $this->containerbox()->addItem($item, $order);
        } catch (ModelNotFoundException $e) {
            $this->createContainerBox();
            $this->addItem($item, $order); // FIXME: may cause recurssive stack overflow
        }
    }

    public function removeItem($item){
        $this->containerbox()->removeItem($item);
    }

    //TODO: OVERWRITE SAVE METHOD TO SAVE CHILDREN

    public function delete()
    {
        try {
            $_box = $this->containerbox();
            parent::delete();
            $_box->delete();
        } catch (ModelNotFoundException $e) {
            // Dont use containerbox...
        }
        parent::delete();
    }

}