<?php

namespace KoalaCMS\Core;

class Paginable extends \KoalaCMS\Core\Publishable{

    public function scopePaginated($query, $size = 10, $page = 1){
        if (isset($_GET['p']))
            $page = intval($_GET['p']);
        if($page < 1)
            $page = 1;
        $page--;
        return $query->skip($page * $size)->take($size);
    }

}