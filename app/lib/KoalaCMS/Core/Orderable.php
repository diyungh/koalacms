<?php

namespace KoalaCMS\Core;

class Orderable extends \KoalaCMS\Core\Model{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('order')->unsigned()->default(0);
    }

    public function scopeOrdered($query, $column = 'order', $order = 'ASC'){
        return $query->orderBy($column, $order);
    }

}