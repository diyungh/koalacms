<?php

namespace KoalaCMS\Core;

class Container extends \KoalaCMS\Core\Slugged{
    protected $table = 'container';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->string('title');
        $table->text('description')->nullable();
        $table->string('child_class');
    }

    public function scopeNotFilter($query, $test){
        return $query->where('title', $not.'like', '%' . $test .'%')
                     ->orWhere('description', $not.'like', '%' . $test .'%');
    }

    public function scopeFilter($query, $test, $not = ''){
        return $query->where('title', $not.'like', '%' . $test .'%')
                     ->orWhere('description', $not.'like', '%' . $test .'%');
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = array())
    {
        if (!$this->exists){
            $this->child_class = $this->getRealClass();
            $this->slug = $this->slugify($this->title);}
        return parent::save($options);
    }
}