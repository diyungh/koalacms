<?php

namespace KoalaCMS\Core;

class Model extends \Illuminate\Database\Eloquent\Model{
    protected $table = null;
    public $translatable = false;

    public function getApp(){
        return \Koala\Slim::getInstance();
    }

    public function getSchema(&$table){
        $table->increments('id');
        $table->timestamps();
        $table->softDeletes();
    }

    public function getClass(){
        return get_class($this);
    }

    public function getRealClass(){
        $classname = get_class($this);
        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)){
            $classname = $matches[1];
        }
        return $classname;
    }

    public function buildSchema(){
        // FIXME: Need exception treatment
        $this->getConnection()->getSchemaBuilder()->create($this->table, function($t){
            $this->getSchema($t);
        });
    }

}