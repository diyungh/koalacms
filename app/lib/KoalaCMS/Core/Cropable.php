<?php

namespace KoalaCMS\Core;

class Cropable extends \KoalaCMS\Core\Resizeable{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('offset_left')->nullable();
        $table->integer('offset_top')->nullable();
    }
}