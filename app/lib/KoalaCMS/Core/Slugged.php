<?php

namespace KoalaCMS\Core;

class Slugged extends \KoalaCMS\Core\Paginable{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->string('slug');
    }

    public function scopeBySlug($query, $slug){
        return $query->where('slug', '=', $slug);
    }

    public function url(){
        return join('/', array($this->slugify($this->child_class), $this->slug));
    }

    /** 
      * Credits: http://sourcecookbook.com/en/recipes/8/function-to-slugify-strings-in-php 
      *
      * @param string $text
      * @return string
      */
    public function slugify($text){
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');

        if (function_exists('iconv')){
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }
     
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
     
        if (empty($text)){
            return 'n-a';
        }
     
        return $text;
    }

}