<?php

namespace KoalaCMS\Core;

class ContainerItem extends \KoalaCMS\Core\Orderable{
    protected $table = 'container_item';

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->integer('container_id')->unsigned();
        $table->string('container_type');
        $table->integer('container_box_id')->unsigned();
        $table->foreign('container_box_id')->references('id')->on('container_box');
    }

    public function setItem($item){
        // FIXME: This forces every Class to have a method with the 
        //        same name to acces the child element
        $_itemClassName = $item->getRealClass();
        $_itemMethodName = strtolower($_itemClassName . '_id');
        $_className = "KoalaCMS\ContainerItem\\" . $_itemClassName;
        $_containerItem = new $_className();
        $_containerItem->$_itemMethodName = $item->id;
        $_containerItem->save();

        $this->container_type = $_className;
        $this->container_id = $_containerItem->id;
    }

    public function containerbox(){
        return $this->belongsTo('\KoalaCMS\Core\ContainerBox');
    }

    public function container(){
        return $this->morphTo();
    }

    public function delete()
    {
        $this->container->delete();
        parent::delete();
    }
}