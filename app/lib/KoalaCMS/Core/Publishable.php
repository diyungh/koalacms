<?php

namespace KoalaCMS\Core;

class Publishable extends \KoalaCMS\Core\Model{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->boolean('published')->default(true);
        $table->dateTime('date_available')->nullable();
    }

    public function scopeOrdered($query, $column = 'id', $order = 'DESC'){
        return $query->orderBy($column, $order);
    }

    public function scopeOnlyPublished($query){
        return $query->where('published', '=', '1');
    }

    public function scopeOnlyAvailable($query){
        return $query->where('date_available', '<=', new \DateTime('now'));
    }

    public function save(array $options = array())
    {
        if (!$this->exists){
            $this->date_available = new \DateTime('now');}
        return parent::save($options);
    }

}