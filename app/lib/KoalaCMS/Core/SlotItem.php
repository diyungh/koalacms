<?php

namespace KoalaCMS\Core;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use KoalaCMS\Model\Slot;

class SlotItem extends \KoalaCMS\Core\Model{

    public function slot(){
        return $this->morphOne('KoalaCMS\Model\Slot', 'container');
    }

}