<?php

namespace KoalaCMS\Core;

class Archive extends \KoalaCMS\Core\Container{

    public function getSchema(&$table){
        parent::getSchema($table);
        $table->text('file');
    }

}