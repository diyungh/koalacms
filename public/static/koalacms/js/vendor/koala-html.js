// Generated by CoffeeScript 1.6.3
var A, B, BODY, DIV, FIELDSET, H1, H2, H3, H4, H5, HEAD, HTML, IMG, INPUT, LABEL, LEGEND, LI, P, SMALL, SPAN, TD, TEXTAREA, TR, UL, XML, type, _A, _B, _BODY, _DIV, _FIELDSET, _H1, _H2, _H3, _H4, _H5, _HEAD, _HTML, _IMG, _INPUT, _LABEL, _LEGEND, _LI, _P, _SMALL, _SPAN, _TD, _TEXTAREA, _TR, _UL,
  __slice = [].slice,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

type = function(obj) {
  var classToType, myClass, name, _i, _len, _ref;
  if (obj === void 0 || obj === null) {
    return String(obj);
  }
  classToType = new Object;
  _ref = "Boolean Number String Function Array Date RegExp".split(" ");
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    name = _ref[_i];
    classToType["[object " + name + "]"] = name.toLowerCase();
  }
  myClass = Object.prototype.toString.call(obj);
  if (myClass in classToType) {
    return classToType[myClass];
  }
  return "object";
};

XML = (function() {
  function XML() {
    var arg, c, content, index, key, tag, value, _i, _len, _ref, _ref1, _ref2;
    tag = arguments[0], content = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
    this.tag = tag;
    this.content = content;
    this.isXMLObject = true;
    this.selfclose = false;
    this.attributes = null;
    _ref = this.content;
    for (index in _ref) {
      arg = _ref[index];
      if (type(arg) === 'object' && !('isXMLObject' in arg)) {
        this.attributes = arg;
        this.content.splice(index, 1);
      }
    }
    if (this.tag.slice(-1) === '/') {
      this.selfclose = true;
      this.tag = this.tag.slice(0, -1);
      this.content = '';
    }
    if (!this.attributes) {
      this.attributes = {};
    }
    _ref1 = this.attributes;
    for (key in _ref1) {
      value = _ref1[key];
      this.create_property(key, value);
    }
    _ref2 = this.content;
    for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
      c = _ref2[_i];
      if (type(c) === 'object') {
        this.create_tag(c.tag, c);
      }
    }
  }

  XML.prototype.text = function(str) {
    if (!str) {
      return this.content;
    } else if (str) {
      return this.content = [str];
    }
  };

  XML.prototype.create_property = function(key, value) {
    return this[key] = function(v) {
      if (v) {
        return this.attributes[key] = v;
      } else if (!v) {
        return this.attributes[key];
      }
    };
  };

  XML.prototype.create_tag = function(tag, xml_object) {
    if (tag in this) {
      if (type(this[tag]) === 'array') {
        return this[tag].push(xml_object);
      } else {
        return this[tag] = [this[tag], xml_object];
      }
    } else if (!(tag in this)) {
      return this[tag] = xml_object;
    }
  };

  XML.prototype._convert_attributes = function() {
    var key, value, values;
    values = (function() {
      var _ref, _results;
      _ref = this.attributes;
      _results = [];
      for (key in _ref) {
        value = _ref[key];
        value = type(value) === 'array' ? value.join(" ") : value;
        if (value) {
          _results.push("" + key + "=\"" + value + "\"");
        } else {
          _results.push(key);
        }
      }
      return _results;
    }).call(this);
    return this.converted_attributes = this.attributes ? " " + (values.join(" ")) : "";
  };

  XML.prototype._convert_content = function() {
    var c, contents;
    contents = (function() {
      var _i, _len, _ref, _results;
      _ref = this.content;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        c = _ref[_i];
        if (type(c) === 'object') {
          _results.push(c.xml());
        } else {
          _results.push(c);
        }
      }
      return _results;
    }).call(this);
    return this.converted_content = contents.join(' ');
  };

  XML.prototype.xml = function() {
    this._convert_attributes();
    this._convert_content();
    if (this.selfclose === true) {
      return "<" + this.tag + this.converted_attributes + "/>";
    } else if (this.selfclose === false) {
      return "<" + this.tag + this.converted_attributes + ">" + this.converted_content + "</" + this.tag + ">";
    }
  };

  return XML;

})();

_HTML = (function(_super) {
  __extends(_HTML, _super);

  function _HTML() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _HTML.__super__.constructor.apply(this, ["html"].concat(__slice.call(args)));
  }

  return _HTML;

})(XML);

_HEAD = (function(_super) {
  __extends(_HEAD, _super);

  function _HEAD() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _HEAD.__super__.constructor.apply(this, ["head"].concat(__slice.call(args)));
  }

  return _HEAD;

})(XML);

_BODY = (function(_super) {
  __extends(_BODY, _super);

  function _BODY() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _BODY.__super__.constructor.apply(this, ["body"].concat(__slice.call(args)));
  }

  return _BODY;

})(XML);

_DIV = (function(_super) {
  __extends(_DIV, _super);

  function _DIV() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _DIV.__super__.constructor.apply(this, ["div"].concat(__slice.call(args)));
  }

  return _DIV;

})(XML);

_H1 = (function(_super) {
  __extends(_H1, _super);

  function _H1() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _H1.__super__.constructor.apply(this, ["h1"].concat(__slice.call(args)));
  }

  return _H1;

})(XML);

_H2 = (function(_super) {
  __extends(_H2, _super);

  function _H2() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _H2.__super__.constructor.apply(this, ["h2"].concat(__slice.call(args)));
  }

  return _H2;

})(XML);

_H3 = (function(_super) {
  __extends(_H3, _super);

  function _H3() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _H3.__super__.constructor.apply(this, ["h3"].concat(__slice.call(args)));
  }

  return _H3;

})(XML);

_H4 = (function(_super) {
  __extends(_H4, _super);

  function _H4() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _H4.__super__.constructor.apply(this, ["h4"].concat(__slice.call(args)));
  }

  return _H4;

})(XML);

_H5 = (function(_super) {
  __extends(_H5, _super);

  function _H5() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _H5.__super__.constructor.apply(this, ["h5"].concat(__slice.call(args)));
  }

  return _H5;

})(XML);

_UL = (function(_super) {
  __extends(_UL, _super);

  function _UL() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _UL.__super__.constructor.apply(this, ["ul"].concat(__slice.call(args)));
  }

  return _UL;

})(XML);

_LI = (function(_super) {
  __extends(_LI, _super);

  function _LI() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _LI.__super__.constructor.apply(this, ["li"].concat(__slice.call(args)));
  }

  return _LI;

})(XML);

_A = (function(_super) {
  __extends(_A, _super);

  function _A() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _A.__super__.constructor.apply(this, ["a"].concat(__slice.call(args)));
  }

  return _A;

})(XML);

_P = (function(_super) {
  __extends(_P, _super);

  function _P() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _P.__super__.constructor.apply(this, ["p"].concat(__slice.call(args)));
  }

  return _P;

})(XML);

_B = (function(_super) {
  __extends(_B, _super);

  function _B() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _B.__super__.constructor.apply(this, ["b"].concat(__slice.call(args)));
  }

  return _B;

})(XML);

_SPAN = (function(_super) {
  __extends(_SPAN, _super);

  function _SPAN() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _SPAN.__super__.constructor.apply(this, ["span"].concat(__slice.call(args)));
  }

  return _SPAN;

})(XML);

_SMALL = (function(_super) {
  __extends(_SMALL, _super);

  function _SMALL() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _SMALL.__super__.constructor.apply(this, ["small"].concat(__slice.call(args)));
  }

  return _SMALL;

})(XML);

_IMG = (function(_super) {
  __extends(_IMG, _super);

  function _IMG() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _IMG.__super__.constructor.apply(this, ["img/"].concat(__slice.call(args)));
  }

  return _IMG;

})(XML);

_TR = (function(_super) {
  __extends(_TR, _super);

  function _TR() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _TR.__super__.constructor.apply(this, ["tr"].concat(__slice.call(args)));
  }

  return _TR;

})(XML);

_TD = (function(_super) {
  __extends(_TD, _super);

  function _TD() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _TD.__super__.constructor.apply(this, ["td"].concat(__slice.call(args)));
  }

  return _TD;

})(XML);

_LEGEND = (function(_super) {
  __extends(_LEGEND, _super);

  function _LEGEND() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _LEGEND.__super__.constructor.apply(this, ["legend"].concat(__slice.call(args)));
  }

  return _LEGEND;

})(XML);

_FIELDSET = (function(_super) {
  __extends(_FIELDSET, _super);

  function _FIELDSET() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _FIELDSET.__super__.constructor.apply(this, ["fieldset"].concat(__slice.call(args)));
  }

  return _FIELDSET;

})(XML);

_LABEL = (function(_super) {
  __extends(_LABEL, _super);

  function _LABEL() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _LABEL.__super__.constructor.apply(this, ["label"].concat(__slice.call(args)));
  }

  return _LABEL;

})(XML);

_INPUT = (function(_super) {
  __extends(_INPUT, _super);

  function _INPUT() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _INPUT.__super__.constructor.apply(this, ["input/"].concat(__slice.call(args)));
  }

  return _INPUT;

})(XML);

_TEXTAREA = (function(_super) {
  __extends(_TEXTAREA, _super);

  function _TEXTAREA() {
    var args;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _TEXTAREA.__super__.constructor.apply(this, ["textarea"].concat(__slice.call(args)));
  }

  return _TEXTAREA;

})(XML);

HTML = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_HTML, args, function(){});
};

HEAD = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_HEAD, args, function(){});
};

BODY = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_BODY, args, function(){});
};

DIV = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_DIV, args, function(){});
};

H1 = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_H1, args, function(){});
};

H2 = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_H2, args, function(){});
};

H3 = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_H3, args, function(){});
};

H4 = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_H4, args, function(){});
};

H5 = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_H5, args, function(){});
};

UL = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_UL, args, function(){});
};

LI = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_LI, args, function(){});
};

A = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_A, args, function(){});
};

P = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_P, args, function(){});
};

B = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_B, args, function(){});
};

SPAN = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_SPAN, args, function(){});
};

SMALL = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_SMALL, args, function(){});
};

IMG = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_IMG, args, function(){});
};

TR = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_TR, args, function(){});
};

TD = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_TD, args, function(){});
};

LEGEND = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_LEGEND, args, function(){});
};

FIELDSET = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_FIELDSET, args, function(){});
};

LABEL = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_LABEL, args, function(){});
};

INPUT = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_INPUT, args, function(){});
};

TEXTAREA = function() {
  var args;
  args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  return (function(func, args, ctor) {
    ctor.prototype = func.prototype;
    var child = new ctor, result = func.apply(child, args);
    return Object(result) === result ? result : child;
  })(_TEXTAREA, args, function(){});
};
