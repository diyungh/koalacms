/*Simple Weather*/
function runWeather(){
    var handler = $('.weather');
    $.simpleWeather({
        woeid: '455827',
        unit: 'c',
        success: function(weather) {
          html = "{3}{0}º - {1}º{2}SÃO PAULO - SP".format(
            weather.high, 
            weather.low, 
            new SPAN({class: 'divider'}).xml(),
            new IMG({width: 21, height: 21, src: "../img/weather/{0}.png".format(weather.code)}).xml())
          handler.html(html);},
        error: function(error) {
          handler.html('Previsão indisponível');}
  });
}

/* ==========================================================================
   Clock
   ========================================================================== */

var clockwork = (function(){
    var self = this;

    self.handler = $('.main_header__menu--info .date');
    self.dateDisplay = self.handler;
    self.pattern = "{0} DE {1} DE {2}";

    self.clockInterval = 1 * 1000;
    self.describeMonth = true;
    self.months = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'];

    self._0 = function(n, l){
        l = l? l: 1;
        return n.length == l? '0' + n: n;}

    self.clocktime = function(){
        var date = new Date(),
            hours = date.getHours().toString(),
            minutes = date.getMinutes().toString();

        hours = self._0(hours);
        minutes = self._0(minutes);

        return hours + ':' + minutes;};

    self.clockdate = function(){
        var date = new Date(),
            days = date.getDate().toString(),
            months = null,
            year = date.getFullYear().toString();

        days = self._0(days);

        /* TODO: Verbose Day name (weekday) */
        /* Verbose month name */
        if(self.describeMonth == true)
            months = self.months[date.getMonth()].toUpperCase()
        else
            months = self._0((date.getMonth()+1).toString())

        return self.pattern.format(days, months, year);};

    self.refresh = function(){
        //self.timeDisplay.html(self.clocktime());
        self.dateDisplay.html(self.clockdate());};

    self.__init__ = function(){
        self.refresh();
        setInterval(self.refresh, self.clockInterval);}

    return self;
})();

/* Horizontal Custom Slider - No effects */
var HorizontalCustomSlider = function(element){
    var self = element,
        config = window['horizontal-custom-slider.{0}'.format(self.data('selector'))];
        
    self.items = config.items;
    self.viewall = config['view-all'];
    // System Vars
    self.currentItem = 0,
    self.cycleInterval = 5 * 1000,
    // Flags
    self.hasPagination = self.items.length > 1;
    self.hasPrev = function(){return (self.currentItem > 0 && self.hasPagination); };
    self.hasNext = function(){return (self.currentItem < (self.items.length-1) && self.hasPagination); };

    self._get = function(target, selector){
        return target.find(selector);}
    // Getters
    self.getTitle = function(target){
        return self._get(target, '[data-title]');}
    self.getDescription = function(target){
        return self._get(target, '[data-description]');}
    self.getLink = function(target){
        return self._get(target, '[data-link]');}
    // Setters
    self.setTitle = function(target, value){
        self._get(target, '[data-title]').text(value);}
    self.setDescription = function(target, value){
        self._get(target, '[data-description]').text(value);}
    self.setLink = function(target, value){
        self._get(target, '[data-link]').attr('href', value);}

    self._get(self, '[data-view-all]').attr('href', self.viewall);

    // Bindings
    self.render = function(target, reference){
        // Title
        if(reference.hasOwnProperty('title'))
            self.setTitle(target, reference.title);
        // Subtitle
        if(reference.hasOwnProperty('subtitle'))
            self.setSubtitle(target, reference.subtitle);
        // Hat
        if(reference.hasOwnProperty('hat'))
            self.setHat(target, reference.hat);
        // Description
        if(reference.hasOwnProperty('description'))
            self.setDescription(target, reference.description);
        // Image
        if(reference.hasOwnProperty('image'))
            self.setImage(target, reference.image);
        // Link
        if(reference.hasOwnProperty('link'))
            self.setLink(target, reference.link);}

    self.next = function(e){
        e.preventDefault();
        if(self.hasNext()){
            self.currentItem += 1;
            self.render(self, self.items[self.currentItem]);}}

    self.previous = function(e){
        e.preventDefault();
        if(self.hasPrev()){
            self.currentItem -= 1;
            self.render(self, self.items[self.currentItem]);}}

    self.find('[data-next]').on('click', self.next);
    self.find('[data-prev]').on('click', self.previous);

};

function runSlider(){
    /* Core elements: 
            Title
            Subtitle
            Hat
            Description
            Image
            Link
    */

    // Helper
    Object.prototype.hasOwnProperty = function(property) {
        return this[property] !== undefined;
    };

    var self = this,
        sliders = $('[data-js-horizontal-slider]');

    sliders.each(function(index, item){
        new HorizontalCustomSlider($(item));
    });}

/*Boot*/
(function boot(){
    /* Start Weather */
    runWeather();
    /* Clock */
    clockwork.__init__();
    /* Last News Slider */
    runSlider();
})();